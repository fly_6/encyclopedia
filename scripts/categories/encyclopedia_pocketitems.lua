-- Cards, Runes, and Soul Stones
Encyclopedia.AddCard({ -- 1.CARD_FOOL
    Class = "vanilla",
    ID = Card.CARD_FOOL,
    WikiDesc = Encyclopedia.CardsWiki.CARD_FOOL,
})
Encyclopedia.AddCard({ -- 2.CARD_MAGICIAN
    Class = "vanilla",
    ID = Card.CARD_MAGICIAN,
    WikiDesc = Encyclopedia.CardsWiki.CARD_MAGICIAN,
})
Encyclopedia.AddCard({ -- 3.CARD_HIGH_PRIESTESS
    Class = "vanilla",
    ID = Card.CARD_HIGH_PRIESTESS,
    WikiDesc = Encyclopedia.CardsWiki.CARD_HIGH_PRIESTESS,
})
Encyclopedia.AddCard({ -- 4.CARD_EMPRESS
    Class = "vanilla",
    ID = Card.CARD_EMPRESS,
    WikiDesc = Encyclopedia.CardsWiki.CARD_EMPRESS,
})
Encyclopedia.AddCard({ -- 5.CARD_EMPEROR
    Class = "vanilla",
    ID = Card.CARD_EMPEROR,
    WikiDesc = Encyclopedia.CardsWiki.CARD_EMPEROR,
})
Encyclopedia.AddCard({ -- 6.CARD_HIEROPHANT
    Class = "vanilla",
    ID = Card.CARD_HIEROPHANT,
    WikiDesc = Encyclopedia.CardsWiki.CARD_HIEROPHANT,
})
Encyclopedia.AddCard({ -- 7.CARD_LOVERS
    Class = "vanilla",
    ID = Card.CARD_LOVERS,
    WikiDesc = Encyclopedia.CardsWiki.CARD_LOVERS,
})
Encyclopedia.AddCard({ -- 8.CARD_CHARIOT
    Class = "vanilla",
    ID = Card.CARD_CHARIOT,
    WikiDesc = Encyclopedia.CardsWiki.CARD_CHARIOT,
})
Encyclopedia.AddCard({ -- 9.CARD_JUSTICE
    Class = "vanilla",
    ID = Card.CARD_JUSTICE,
    WikiDesc = Encyclopedia.CardsWiki.CARD_JUSTICE,
})
Encyclopedia.AddCard({ -- 10.CARD_HERMIT
    Class = "vanilla",
    ID = Card.CARD_HERMIT,
    WikiDesc = Encyclopedia.CardsWiki.CARD_HERMIT,
})
Encyclopedia.AddCard({ -- 11.CARD_WHEEL_OF_FORTUNE
    Class = "vanilla",
    ID = Card.CARD_WHEEL_OF_FORTUNE,
    WikiDesc = Encyclopedia.CardsWiki.CARD_WHEEL_OF_FORTUNE,
})
Encyclopedia.AddCard({ -- 12.CARD_STRENGTH
    Class = "vanilla",
    ID = Card.CARD_STRENGTH,
    WikiDesc = Encyclopedia.CardsWiki.CARD_STRENGTH,
})
Encyclopedia.AddCard({ -- 13.CARD_HANGED_MAN
    Class = "vanilla",
    ID = Card.CARD_HANGED_MAN,
    WikiDesc = Encyclopedia.CardsWiki.CARD_HANGED_MAN,
})
Encyclopedia.AddCard({ -- 14.CARD_DEATH
    Class = "vanilla",
    ID = Card.CARD_DEATH,
    WikiDesc = Encyclopedia.CardsWiki.CARD_DEATH,
})
Encyclopedia.AddCard({ -- 15.CARD_TEMPERANCE
    Class = "vanilla",
    ID = Card.CARD_TEMPERANCE,
    WikiDesc = Encyclopedia.CardsWiki.CARD_TEMPERANCE,
})
Encyclopedia.AddCard({ -- 16.CARD_DEVIL
    Class = "vanilla",
    ID = Card.CARD_DEVIL,
    WikiDesc = Encyclopedia.CardsWiki.CARD_DEVIL,
})
Encyclopedia.AddCard({ -- 17.CARD_TOWER
    Class = "vanilla",
    ID = Card.CARD_TOWER,
    WikiDesc = Encyclopedia.CardsWiki.CARD_TOWER,
})
Encyclopedia.AddCard({ -- 18.CARD_STARS
    Class = "vanilla",
    ID = Card.CARD_STARS,
    WikiDesc = Encyclopedia.CardsWiki.CARD_STARS,
})
Encyclopedia.AddCard({ -- 19.CARD_MOON
    Class = "vanilla",
    ID = Card.CARD_MOON,
    WikiDesc = Encyclopedia.CardsWiki.CARD_MOON,
})
Encyclopedia.AddCard({ -- 20.CARD_SUN
    Class = "vanilla",
    ID = Card.CARD_SUN,
    WikiDesc = Encyclopedia.CardsWiki.CARD_SUN,
})
Encyclopedia.AddCard({ -- 21.CARD_JUDGEMENT
    Class = "vanilla",
    ID = Card.CARD_JUDGEMENT,
    WikiDesc = Encyclopedia.CardsWiki.CARD_JUDGEMENT,
})
Encyclopedia.AddCard({ -- 22.CARD_WORLD
    Class = "vanilla",
    ID = Card.CARD_WORLD,
    WikiDesc = Encyclopedia.CardsWiki.CARD_WORLD,
})
Encyclopedia.AddCard({ -- 23.CARD_CLUBS_2
    Class = "vanilla",
    ID = Card.CARD_CLUBS_2,
    WikiDesc = Encyclopedia.CardsWiki.CARD_CLUBS_2,
})
Encyclopedia.AddCard({ -- 24.CARD_DIAMONDS_2
    Class = "vanilla",
    ID = Card.CARD_DIAMONDS_2,
    WikiDesc = Encyclopedia.CardsWiki.CARD_DIAMONDS_2,
})
Encyclopedia.AddCard({ -- 25.CARD_SPADES_2
    Class = "vanilla",
    ID = Card.CARD_SPADES_2,
    WikiDesc = Encyclopedia.CardsWiki.CARD_SPADES_2,
})
Encyclopedia.AddCard({ -- 26.CARD_HEARTS_2
    Class = "vanilla",
    ID = Card.CARD_HEARTS_2,
    WikiDesc = Encyclopedia.CardsWiki.CARD_HEARTS_2,
})
Encyclopedia.AddCard({ -- 27.CARD_ACE_OF_CLUBS
    Class = "vanilla",
    ID = Card.CARD_ACE_OF_CLUBS,
    WikiDesc = Encyclopedia.CardsWiki.CARD_ACE_OF_CLUBS,
})
Encyclopedia.AddCard({ -- 28.CARD_ACE_OF_DIAMONDS
    Class = "vanilla",
    ID = Card.CARD_ACE_OF_DIAMONDS,
    WikiDesc = Encyclopedia.CardsWiki.CARD_ACE_OF_DIAMONDS,
})
Encyclopedia.AddCard({ -- 29.CARD_ACE_OF_SPADES
    Class = "vanilla",
    ID = Card.CARD_ACE_OF_SPADES,
    WikiDesc = Encyclopedia.CardsWiki.CARD_ACE_OF_SPADES,
})
Encyclopedia.AddCard({ -- 30.CARD_ACE_OF_HEARTS
    Class = "vanilla",
    ID = Card.CARD_ACE_OF_HEARTS,
    WikiDesc = Encyclopedia.CardsWiki.CARD_ACE_OF_HEARTS,
})
Encyclopedia.AddCard({ -- 31.CARD_JOKER
    Class = "vanilla",
    ID = Card.CARD_JOKER,
    WikiDesc = Encyclopedia.CardsWiki.CARD_JOKER,
})
Encyclopedia.AddRune({ -- 32.RUNE_HAGALAZ
    Class = "vanilla",
    ID = Card.RUNE_HAGALAZ,
    WikiDesc = Encyclopedia.RunesWiki.RUNE_HAGALAZ,
    RuneType = 0,
})
Encyclopedia.AddRune({ -- 33.RUNE_JERA
    Class = "vanilla",
    ID = Card.RUNE_JERA,
    WikiDesc = Encyclopedia.RunesWiki.RUNE_JERA,
	RuneType = 0,
})
Encyclopedia.AddRune({ -- 34.RUNE_EHWAZ
    Class = "vanilla",
    ID = Card.RUNE_EHWAZ,
    WikiDesc = Encyclopedia.RunesWiki.RUNE_EHWAZ,
    RuneType = 0,
})
Encyclopedia.AddRune({ -- 35.RUNE_DAGAZ
    Class = "vanilla",
    ID = Card.RUNE_DAGAZ,
    WikiDesc = Encyclopedia.RunesWiki.RUNE_DAGAZ,
    RuneType = 0,
})
Encyclopedia.AddRune({ -- 36.RUNE_ANSUZ
    Class = "vanilla",
    ID = Card.RUNE_ANSUZ,
    WikiDesc = Encyclopedia.RunesWiki.RUNE_ANSUZ,
    RuneType = 1,
})
Encyclopedia.AddRune({ -- 37.RUNE_PERTHRO
    Class = "vanilla",
    ID = Card.RUNE_PERTHRO,
    WikiDesc = Encyclopedia.RunesWiki.RUNE_PERTHRO,
    RuneType = 1,
})
Encyclopedia.AddRune({ -- 38.RUNE_BERKANO
    Class = "vanilla",
    ID = Card.RUNE_BERKANO,
    WikiDesc = Encyclopedia.RunesWiki.RUNE_BERKANO,
    RuneType = 1,
})
Encyclopedia.AddRune({ -- 39.RUNE_ALGIZ
    Class = "vanilla",
    ID = Card.RUNE_ALGIZ,
    WikiDesc = Encyclopedia.RunesWiki.RUNE_ALGIZ,
    RuneType = 1,
})
Encyclopedia.AddRune({ -- 40.RUNE_BLANK
    Class = "vanilla",
    ID = Card.RUNE_BLANK,
    WikiDesc = Encyclopedia.RunesWiki.RUNE_BLANK,
    RuneType = 1,
})
Encyclopedia.AddRune({ -- 41.RUNE_BLACK
    Class = "vanilla",
    ID = Card.RUNE_BLACK,
    WikiDesc = Encyclopedia.RunesWiki.RUNE_BLACK,
    RuneType = 2,
})
Encyclopedia.AddCard({ -- 42.CARD_CHAOS
    Class = "vanilla",
    ID = Card.CARD_CHAOS,
    WikiDesc = Encyclopedia.CardsWiki.CARD_CHAOS,
})
Encyclopedia.AddCard({ -- 43.CARD_CREDIT
    Class = "vanilla",
    ID = Card.CARD_CREDIT,
    WikiDesc = Encyclopedia.CardsWiki.CARD_CREDIT,
})
Encyclopedia.AddCard({ -- 44.CARD_RULES
    Class = "vanilla",
    ID = Card.CARD_RULES,
    WikiDesc = Encyclopedia.CardsWiki.CARD_RULES,
})
Encyclopedia.AddCard({ -- 45.CARD_HUMANITY
    Class = "vanilla",
    ID = Card.CARD_HUMANITY,
    WikiDesc = Encyclopedia.CardsWiki.CARD_HUMANITY,
})
Encyclopedia.AddCard({ -- 46.CARD_SUICIDE_KING
    Class = "vanilla",
    ID = Card.CARD_SUICIDE_KING,
    WikiDesc = Encyclopedia.CardsWiki.CARD_SUICIDE_KING,
})
Encyclopedia.AddCard({ -- 47.CARD_GET_OUT_OF_JAIL
    Class = "vanilla",
    ID = Card.CARD_GET_OUT_OF_JAIL,
    WikiDesc = Encyclopedia.CardsWiki.CARD_GET_OUT_OF_JAIL,
})
Encyclopedia.AddCard({ -- 48.CARD_QUESTIONMARK
    Class = "vanilla",
    ID = Card.CARD_QUESTIONMARK,
    WikiDesc = Encyclopedia.CardsWiki.CARD_QUESTIONMARK,
})
Encyclopedia.AddCard({ -- 49.CARD_DICE_SHARD
    Class = "vanilla",
    ID = Card.CARD_DICE_SHARD,
    WikiDesc = Encyclopedia.CardsWiki.CARD_DICE_SHARD,
})
Encyclopedia.AddCard({ -- 50.CARD_EMERGENCY_CONTACT
    Class = "vanilla",
    ID = Card.CARD_EMERGENCY_CONTACT,
    WikiDesc = Encyclopedia.CardsWiki.CARD_EMERGENCY_CONTACT,
})
Encyclopedia.AddCard({ -- 51.CARD_HOLY
    Class = "vanilla",
    ID = Card.CARD_HOLY,
    WikiDesc = Encyclopedia.CardsWiki.CARD_HOLY,
})
Encyclopedia.AddCard({ -- 52.CARD_HUGE_GROWTH
    Class = "vanilla",
    ID = Card.CARD_HUGE_GROWTH,
    WikiDesc = Encyclopedia.CardsWiki.CARD_HUGE_GROWTH,
})
Encyclopedia.AddCard({ -- 53.CARD_ANCIENT_RECALL
    Class = "vanilla",
    ID = Card.CARD_ANCIENT_RECALL,
    WikiDesc = Encyclopedia.CardsWiki.CARD_ANCIENT_RECALL,
})
Encyclopedia.AddCard({ -- 54.CARD_ERA_WALK
    Class = "vanilla",
    ID = Card.CARD_ERA_WALK,
    WikiDesc = Encyclopedia.CardsWiki.CARD_ERA_WALK,
})
if REPENTANCE then
Encyclopedia.AddRune({ -- 55.RUNE_SHARD
    Class = "vanilla",
    ID = Card.RUNE_SHARD,
    WikiDesc = Encyclopedia.RunesWiki.RUNE_SHARD,
    RuneType = 3,
})
Encyclopedia.AddCard({ -- 56.CARD_REVERSE_FOOL
    Class = "vanilla",
    ID = Card.CARD_REVERSE_FOOL,
    WikiDesc = Encyclopedia.CardsWiki.CARD_REVERSE_FOOL,
})
Encyclopedia.AddCard({ -- 57.CARD_REVERSE_MAGICIAN
    Class = "vanilla",
    ID = Card.CARD_REVERSE_MAGICIAN,
    WikiDesc = Encyclopedia.CardsWiki.CARD_REVERSE_MAGICIAN,
})
Encyclopedia.AddCard({ -- 58.CARD_REVERSE_HIGH_PRIESTESS
    Class = "vanilla",
    ID = Card.CARD_REVERSE_HIGH_PRIESTESS,
    WikiDesc = Encyclopedia.CardsWiki.CARD_REVERSE_HIGH_PRIESTESS,
})
Encyclopedia.AddCard({ -- 59.CARD_REVERSE_EMPRESS
    Class = "vanilla",
    ID = Card.CARD_REVERSE_EMPRESS,
    WikiDesc = Encyclopedia.CardsWiki.CARD_REVERSE_EMPRESS,
})
Encyclopedia.AddCard({ -- 60.CARD_REVERSE_EMPEROR
    Class = "vanilla",
    ID = Card.CARD_REVERSE_EMPEROR,
    WikiDesc = Encyclopedia.CardsWiki.CARD_REVERSE_EMPEROR,
})
Encyclopedia.AddCard({ -- 61.CARD_REVERSE_HIEROPHANT
    Class = "vanilla",
    ID = Card.CARD_REVERSE_HIEROPHANT,
    WikiDesc = Encyclopedia.CardsWiki.CARD_REVERSE_HIEROPHANT,
})
Encyclopedia.AddCard({ -- 62.CARD_REVERSE_LOVERS
    Class = "vanilla",
    ID = Card.CARD_REVERSE_LOVERS,
    WikiDesc = Encyclopedia.CardsWiki.CARD_REVERSE_LOVERS,
})
Encyclopedia.AddCard({ -- 63.CARD_REVERSE_CHARIOT
    Class = "vanilla",
    ID = Card.CARD_REVERSE_CHARIOT,
    WikiDesc = Encyclopedia.CardsWiki.CARD_REVERSE_CHARIOT,
})
Encyclopedia.AddCard({ -- 64.CARD_REVERSE_JUSTICE
    Class = "vanilla",
    ID = Card.CARD_REVERSE_JUSTICE,
    WikiDesc = Encyclopedia.CardsWiki.CARD_REVERSE_JUSTICE,
})
Encyclopedia.AddCard({ -- 65.CARD_REVERSE_HERMIT
    Class = "vanilla",
    ID = Card.CARD_REVERSE_HERMIT,
    WikiDesc = Encyclopedia.CardsWiki.CARD_REVERSE_HERMIT,
})
Encyclopedia.AddCard({ -- 66.CARD_REVERSE_WHEEL_OF_FORTUNE
    Class = "vanilla",
    ID = Card.CARD_REVERSE_WHEEL_OF_FORTUNE,
    WikiDesc = Encyclopedia.CardsWiki.CARD_REVERSE_WHEEL_OF_FORTUNE,
})
Encyclopedia.AddCard({ -- 67.CARD_REVERSE_STRENGTH
    Class = "vanilla",
    ID = Card.CARD_REVERSE_STRENGTH,
    WikiDesc = Encyclopedia.CardsWiki.CARD_REVERSE_STRENGTH,
})
Encyclopedia.AddCard({ -- 68.CARD_REVERSE_HANGED_MAN
    Class = "vanilla",
    ID = Card.CARD_REVERSE_HANGED_MAN,
    WikiDesc = Encyclopedia.CardsWiki.CARD_REVERSE_HANGED_MAN,
})
Encyclopedia.AddCard({ -- 69.CARD_REVERSE_DEATH
    Class = "vanilla",
    ID = Card.CARD_REVERSE_DEATH,
    WikiDesc = Encyclopedia.CardsWiki.CARD_REVERSE_DEATH,
})
Encyclopedia.AddCard({ -- 70.CARD_REVERSE_TEMPERANCE
    Class = "vanilla",
    ID = Card.CARD_REVERSE_TEMPERANCE,
    WikiDesc = Encyclopedia.CardsWiki.CARD_REVERSE_TEMPERANCE,
})
Encyclopedia.AddCard({ -- 71.CARD_REVERSE_DEVIL
    Class = "vanilla",
    ID = Card.CARD_REVERSE_DEVIL,
    WikiDesc = Encyclopedia.CardsWiki.CARD_REVERSE_DEVIL,
})
Encyclopedia.AddCard({ -- 72.CARD_REVERSE_TOWER
    Class = "vanilla",
    ID = Card.CARD_REVERSE_TOWER,
    WikiDesc = Encyclopedia.CardsWiki.CARD_REVERSE_TOWER,
})
Encyclopedia.AddCard({ -- 73.CARD_REVERSE_STARS
    Class = "vanilla",
    ID = Card.CARD_REVERSE_STARS,
    WikiDesc = Encyclopedia.CardsWiki.CARD_REVERSE_STARS,
})
Encyclopedia.AddCard({ -- 74.CARD_REVERSE_MOON
    Class = "vanilla",
    ID = Card.CARD_REVERSE_MOON,
    WikiDesc = Encyclopedia.CardsWiki.CARD_REVERSE_MOON,
})
Encyclopedia.AddCard({ -- 75.CARD_REVERSE_SUN
    Class = "vanilla",
    ID = Card.CARD_REVERSE_SUN,
    WikiDesc = Encyclopedia.CardsWiki.CARD_REVERSE_SUN,
})
Encyclopedia.AddCard({ -- 76.CARD_REVERSE_JUDGEMENT
    Class = "vanilla",
    ID = Card.CARD_REVERSE_JUDGEMENT,
    WikiDesc = Encyclopedia.CardsWiki.CARD_REVERSE_JUDGEMENT,
})
Encyclopedia.AddCard({ -- 77.CARD_REVERSE_WORLD
    Class = "vanilla",
    ID = Card.CARD_REVERSE_WORLD,
    WikiDesc = Encyclopedia.CardsWiki.CARD_REVERSE_WORLD,
})
Encyclopedia.AddCard({ -- 78.CARD_CRACKED_KEY
    Class = "vanilla",
    ID = Card.CARD_CRACKED_KEY,
    WikiDesc = Encyclopedia.CardsWiki.CARD_CRACKED_KEY,
})
Encyclopedia.AddCard({ -- 79.CARD_QUEEN_OF_HEARTS
    Class = "vanilla",
    ID = Card.CARD_QUEEN_OF_HEARTS,
    WikiDesc = Encyclopedia.CardsWiki.CARD_QUEEN_OF_HEARTS,
})
Encyclopedia.AddCard({ -- 80.CARD_WILD
    Class = "vanilla",
    ID = Card.CARD_WILD,
    WikiDesc = Encyclopedia.CardsWiki.CARD_WILD,
})
Encyclopedia.AddSoul({ -- 81.CARD_SOUL_ISAAC
    Class = "vanilla",
    ID = Card.CARD_SOUL_ISAAC,
    WikiDesc = Encyclopedia.SoulsWiki.CARD_SOUL_ISAAC,
})
Encyclopedia.AddSoul({ -- 82.CARD_SOUL_MAGDALENE
    Class = "vanilla",
    ID = Card.CARD_SOUL_MAGDALENE,
    WikiDesc = Encyclopedia.SoulsWiki.CARD_SOUL_MAGDALENE,
})
Encyclopedia.AddSoul({ -- 83.CARD_SOUL_CAIN
    Class = "vanilla",
    ID = Card.CARD_SOUL_CAIN,
    WikiDesc = Encyclopedia.SoulsWiki.CARD_SOUL_CAIN,
})
Encyclopedia.AddSoul({ -- 84.CARD_SOUL_JUDAS
    Class = "vanilla",
    ID = Card.CARD_SOUL_JUDAS,
    WikiDesc = Encyclopedia.SoulsWiki.CARD_SOUL_JUDAS,
})
Encyclopedia.AddSoul({ -- 85.CARD_SOUL_BLUEBABY
    Class = "vanilla",
    ID = Card.CARD_SOUL_BLUEBABY,
    WikiDesc = Encyclopedia.SoulsWiki.CARD_SOUL_BLUEBABY,
})
Encyclopedia.AddSoul({ -- 86.CARD_SOUL_EVE
    Class = "vanilla",
    ID = Card.CARD_SOUL_EVE,
    WikiDesc = Encyclopedia.SoulsWiki.CARD_SOUL_EVE,
})
Encyclopedia.AddSoul({ -- 87.CARD_SOUL_SAMSON
    Class = "vanilla",
    ID = Card.CARD_SOUL_SAMSON,
    WikiDesc = Encyclopedia.SoulsWiki.CARD_SOUL_SAMSON,
})
Encyclopedia.AddSoul({ -- 88.CARD_SOUL_AZAZEL
    Class = "vanilla",
    ID = Card.CARD_SOUL_AZAZEL,
    WikiDesc = Encyclopedia.SoulsWiki.CARD_SOUL_AZAZEL,
})
Encyclopedia.AddSoul({ -- 89.CARD_SOUL_LAZARUS
    Class = "vanilla",
    ID = Card.CARD_SOUL_LAZARUS,
    WikiDesc = Encyclopedia.SoulsWiki.CARD_SOUL_LAZARUS,
})
Encyclopedia.AddSoul({ -- 90.CARD_SOUL_EDEN
    Class = "vanilla",
    ID = Card.CARD_SOUL_EDEN,
    WikiDesc = Encyclopedia.SoulsWiki.CARD_SOUL_EDEN,
})
Encyclopedia.AddSoul({ -- 91.CARD_SOUL_LOST
    Class = "vanilla",
    ID = Card.CARD_SOUL_LOST,
    WikiDesc = Encyclopedia.SoulsWiki.CARD_SOUL_LOST,
})
Encyclopedia.AddSoul({ -- 92.CARD_SOUL_LILITH
    Class = "vanilla",
    ID = Card.CARD_SOUL_LILITH,
    WikiDesc = Encyclopedia.SoulsWiki.CARD_SOUL_LILITH,
})
Encyclopedia.AddSoul({ -- 93.CARD_SOUL_KEEPER
    Class = "vanilla",
    ID = Card.CARD_SOUL_KEEPER,
    WikiDesc = Encyclopedia.SoulsWiki.CARD_SOUL_KEEPER,
})
Encyclopedia.AddSoul({ -- 94.CARD_SOUL_APOLLYON
    Class = "vanilla",
    ID = Card.CARD_SOUL_APOLLYON,
    WikiDesc = Encyclopedia.SoulsWiki.CARD_SOUL_APOLLYON,
})
Encyclopedia.AddSoul({ -- 95.CARD_SOUL_FORGOTTEN
    Class = "vanilla",
    ID = Card.CARD_SOUL_FORGOTTEN,
    WikiDesc = Encyclopedia.SoulsWiki.CARD_SOUL_FORGOTTEN,
})
Encyclopedia.AddSoul({ -- 96.CARD_SOUL_BETHANY
    Class = "vanilla",
    ID = Card.CARD_SOUL_BETHANY,
    WikiDesc = Encyclopedia.SoulsWiki.CARD_SOUL_BETHANY,
})
Encyclopedia.AddSoul({ -- 97.CARD_SOUL_JACOB
    Class = "vanilla",
    ID = Card.CARD_SOUL_JACOB,
    WikiDesc = Encyclopedia.SoulsWiki.CARD_SOUL_JACOB,
})
end

-- Pills
Encyclopedia.AddPill({ -- 0.PILLEFFECT_BAD_GAS
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_BAD_GAS,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_BAD_GAS,
	Color = 1,
	Description = "Blegh",
})
Encyclopedia.AddPill({ -- 1.PILLEFFECT_BAD_TRIP
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_BAD_TRIP,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_BAD_TRIP,
	Color = 4,
	Description = "Negative",
})
Encyclopedia.AddPill({ -- 2.PILLEFFECT_BALLS_OF_STEEL
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_BALLS_OF_STEEL,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_BALLS_OF_STEEL,
	Color = 9,
	Description = "Positive",
})
Encyclopedia.AddPill({ -- 3.PILLEFFECT_BOMBS_ARE_KEYS
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_BOMBS_ARE_KEYS,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_BOMBS_ARE_KEYS,
	Color = 10,
	Description = "Neutral",
})
Encyclopedia.AddPill({ -- 4.PILLEFFECT_EXPLOSIVE_DIARRHEA
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_EXPLOSIVE_DIARRHEA,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_EXPLOSIVE_DIARRHEA,
	Color = 10,
	Description = "Neutral",
})
Encyclopedia.AddPill({ -- 5.PILLEFFECT_FULL_HEALTH
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_FULL_HEALTH,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_FULL_HEALTH,
	Color = 9,
	Description = "Positive",
})
Encyclopedia.AddPill({ -- 6.PILLEFFECT_HEALTH_DOWN
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_HEALTH_DOWN,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_HEALTH_DOWN,
	Color = 4,
	Description = "Negative",
})
Encyclopedia.AddPill({ -- 7.PILLEFFECT_HEALTH_UP
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_HEALTH_UP,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_HEALTH_UP,
	Color = 9,
	Description = "Positive",
})
Encyclopedia.AddPill({ -- 8.PILLEFFECT_I_FOUND_PILLS
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_I_FOUND_PILLS,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_I_FOUND_PILLS,
	Color = 10,
	Description = "Neutral",
})
Encyclopedia.AddPill({ -- 9.PILLEFFECT_PUBERTY
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_PUBERTY,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_PUBERTY,
	Color = 1,
	Description = "Blegh",
})
Encyclopedia.AddPill({ -- 10.PILLEFFECT_PRETTY_FLY
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_PRETTY_FLY,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_PRETTY_FLY,
	Color = 9,
	Description = "Positive",
})
Encyclopedia.AddPill({ -- 11.PILLEFFECT_RANGE_DOWN
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_RANGE_DOWN,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_RANGE_DOWN,
	Color = 4,
	Description = "Negative",
})
Encyclopedia.AddPill({ -- 12.PILLEFFECT_RANGE_UP
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_RANGE_UP,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_RANGE_UP,
	Color = 9,
	Description = "Positive",
})
Encyclopedia.AddPill({ -- 13.PILLEFFECT_SPEED_DOWN
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_SPEED_DOWN,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_SPEED_DOWN,
	Color = 4,
	Description = "Negative",
})
Encyclopedia.AddPill({ -- 14.PILLEFFECT_SPEED_UP
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_SPEED_UP,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_SPEED_UP,
	Color = 9,
	Description = "Positive",
})
Encyclopedia.AddPill({ -- 15.PILLEFFECT_TEARS_DOWN
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_TEARS_DOWN,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_TEARS_DOWN,
	Color = 4,
	Description = "Negative",
})
Encyclopedia.AddPill({ -- 16.PILLEFFECT_TEARS_UP
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_TEARS_UP,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_TEARS_UP,
	Color = 9,
	Description = "Positive",
})
Encyclopedia.AddPill({ -- 17.PILLEFFECT_LUCK_DOWN
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_LUCK_DOWN,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_LUCK_DOWN,
	Color = 4,
	Description = "Negative",
})
Encyclopedia.AddPill({ -- 18.PILLEFFECT_LUCK_UP
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_LUCK_UP,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_LUCK_UP,
	Color = 9,
	Description = "Positive",
})
Encyclopedia.AddPill({ -- 19.PILLEFFECT_TELEPILLS
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_TELEPILLS,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_TELEPILLS,
	Color = 10,
	Description = "Neutral",
})
Encyclopedia.AddPill({ -- 20.PILLEFFECT_48HOUR_ENERGY
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_48HOUR_ENERGY,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_48HOUR_ENERGY,
	Color = 9,
	Description = "Positive",
})
Encyclopedia.AddPill({ -- 21.PILLEFFECT_HEMATEMESIS
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_HEMATEMESIS,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_HEMATEMESIS,
	Color = 10,
	Description = "Neutral",
})
Encyclopedia.AddPill({ -- 22.PILLEFFECT_PARALYSIS
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_PARALYSIS,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_PARALYSIS,
	Color = 4,
	Description = "Negative",
})
Encyclopedia.AddPill({ -- 23.PILLEFFECT_SEE_FOREVER
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_SEE_FOREVER,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_SEE_FOREVER,
	Color = 9,
	Description = "Positive",
})
Encyclopedia.AddPill({ -- 24.PILLEFFECT_PHEROMONES
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_PHEROMONES,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_PHEROMONES,
	Color = 1,
	Description = "Blegh",
})
Encyclopedia.AddPill({ -- 25.PILLEFFECT_AMNESIA
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_AMNESIA,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_AMNESIA,
	Color = 4,
	Description = "Negative",
})
Encyclopedia.AddPill({ -- 26.PILLEFFECT_LEMON_PARTY
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_LEMON_PARTY,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_LEMON_PARTY,
	Color = 9,
	Description = "Positive",
})
Encyclopedia.AddPill({ -- 27.PILLEFFECT_WIZARD
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_WIZARD,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_WIZARD,
	Color = 4,
	Description = "Negative",
})
Encyclopedia.AddPill({ -- 28.PILLEFFECT_PERCS
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_PERCS,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_PERCS,
	Color = 9,
	Description = "Positive",
})
Encyclopedia.AddPill({ -- 29.PILLEFFECT_ADDICTED
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_ADDICTED,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_ADDICTED,
	Color = 4,
	Description = "Negative",
})
Encyclopedia.AddPill({ -- 30.PILLEFFECT_RELAX
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_RELAX,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_RELAX,
	Color = 1,
	Description = "Blegh",
})
Encyclopedia.AddPill({ -- 31.PILLEFFECT_QUESTIONMARK
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_QUESTIONMARK,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_QUESTIONMARK,
	Color = 4,
	Description = "Negative",
})
Encyclopedia.AddPill({ -- 32.PILLEFFECT_LARGER
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_LARGER,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_LARGER,
	Color = 4,
	Description = "Negative",
})
Encyclopedia.AddPill({ -- 33.PILLEFFECT_SMALLER
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_SMALLER,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_SMALLER,
	Color = 9,
	Description = "Positive",
})
Encyclopedia.AddPill({ -- 34.PILLEFFECT_INFESTED_EXCLAMATION
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_INFESTED_EXCLAMATION,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_INFESTED_EXCLAMATION,
	Color = 1,
	Description = "Blegh",
})
Encyclopedia.AddPill({ -- 35.PILLEFFECT_INFESTED_QUESTION
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_INFESTED_QUESTION,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_INFESTED_QUESTION,
	Color = 1,
	Description = "Blegh",
})
Encyclopedia.AddPill({ -- 36.PILLEFFECT_POWER
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_POWER,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_POWER,
	Color = 9,
	Description = "Positive",
})
Encyclopedia.AddPill({ -- 37.PILLEFFECT_RETRO_VISION
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_RETRO_VISION,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_RETRO_VISION,
	Color = 4,
	Description = "Negative",
})
Encyclopedia.AddPill({ -- 38.PILLEFFECT_FRIENDS_TILL_THE_END
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_FRIENDS_TILL_THE_END,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_FRIENDS_TILL_THE_END,
	Color = 1,
	Description = "Blegh",
})
Encyclopedia.AddPill({ -- 39.PILLEFFECT_X_LAX
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_X_LAX,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_X_LAX,
	Color = 1,
	Description = "Blegh",
})
Encyclopedia.AddPill({ -- 40.PILLEFFECT_SOMETHINGS_WRONG
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_SOMETHINGS_WRONG,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_SOMETHINGS_WRONG,
	Color = 1,
	Description = "Blegh",
})
Encyclopedia.AddPill({ -- 41.PILLEFFECT_IM_DROWSY
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_IM_DROWSY,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_IM_DROWSY,
	Color = 9,
	Description = "Positive",
})
Encyclopedia.AddPill({ -- 42.PILLEFFECT_IM_EXCITED
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_IM_EXCITED,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_IM_EXCITED,
	Color = 4,
	Description = "Negative",
})
Encyclopedia.AddPill({ -- 43.PILLEFFECT_GULP
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_GULP,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_GULP,
	Color = 9,
	Description = "Positive",
})
Encyclopedia.AddPill({ -- 44.PILLEFFECT_HORF
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_HORF,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_HORF,
	Color = 1,
	Description = "Blegh",
})
Encyclopedia.AddPill({ -- 45.PILLEFFECT_SUNSHINE
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_SUNSHINE,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_SUNSHINE,
	Color = 9,
	Description = "Positive",
})
Encyclopedia.AddPill({ -- 46.PILLEFFECT_VURP
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_VURP,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_VURP,
	Color = 9,
	Description = "Positive",
})
if REPENTANCE then
Encyclopedia.AddPill({ -- 47.PILLEFFECT_SHOT_SPEED_DOWN
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_SHOT_SPEED_DOWN,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_SHOT_SPEED_DOWN,
	Color = 4,
	Description = "Negative",
})
Encyclopedia.AddPill({ -- 48.PILLEFFECT_SHOT_SPEED_UP
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_SHOT_SPEED_UP,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_SHOT_SPEED_UP,
	Color = 9,
	Description = "Positive",
})
Encyclopedia.AddPill({ -- 49.PILLEFFECT_EXPERIMENTAL
    Class = "vanilla",
    ID = PillEffect.PILLEFFECT_EXPERIMENTAL,
    WikiDesc = Encyclopedia.PillsWiki.PILLEFFECT_EXPERIMENTAL,
	Color = 10,
	Description = "Neutral",
})
end