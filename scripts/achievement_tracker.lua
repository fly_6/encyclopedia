local game = Game()

local Unlocks = {
	MomsHeart = {Unlock = false, Hard = false},
	Isaac = {Unlock = false, Hard = false},
	Satan = {Unlock = false, Hard = false},
	BlueBaby = {Unlock = false, Hard = false},
	Lamb = {Unlock = false, Hard = false},
	BossRush = {Unlock = false, Hard = false},
	Hush = {Unlock = false, Hard = false},
	MegaSatan = {Unlock = false, Hard = false},
	Delirium = {Unlock = false, Hard = false},
	Mother = {Unlock = false, Hard = false},
	Beast = {Unlock = false, Hard = false},
	GreedMode = {Unlock = false, Hard = false},
}

local function UpdateCompletionMarks(tab, name)
	local difficulty = game.Difficulty

	if tab[name].Unlock == false then
		tab[name].Unlock = true
	end
	
	if difficulty == Difficulty.DIFFICULTY_HARD
	or difficulty == Difficulty.DIFFICULTY_GREEDIER then
		tab[name].Hard = true
	end
end

local SwitchFuncs = {
	["Esau"] = function(pType)
		return "Jacob and Esau", PlayerType.PLAYER_JACOB
	end,
	["Lazarus"] = function(pType)
		if pType == PlayerType.PLAYER_LAZARUS2_B then
			return "Lazarus", PlayerType.PLAYER_LAZARUS_B
		end
	end,
	["Lazarus II"] = function(pType)
		return "Lazarus", PlayerType.PLAYER_LAZARUS
	end,
	["Black Judas"] = function(pType)
		return "Judas", PlayerType.PLAYER_JUDAS
	end,
	["The Soul"] = function(pType)
		if pType == PlayerType.PLAYER_THESOUL_B then
			return "The Forgotten", PlayerType.PLAYER_THEFORGOTTEN_B
		else
			return "The Forgotten", PlayerType.PLAYER_THEFORGOTTEN
		end
	end,
}

local function CompletionFunc(name)
	for p = 0, game:GetNumPlayers() - 1 do
		local player = Isaac.GetPlayer(p)
		--local pName = string.gsub(player:GetName(), "Tainted ", "")
		local pName = player:GetName()
		local pType = player:GetPlayerType()
		
		if SwitchFuncs[pName] then
			pName, pType = SwitchFuncs[pName](pType)
		end
		
		if Encyclopedia.CompletionNotes_a[pName]
		and Encyclopedia.TrackerTables.characters_aNames[pType] then
			UpdateCompletionMarks(Encyclopedia.CompletionNotes_a[pName].Unlocks, name)
			
		elseif Encyclopedia.CompletionNotes_b[pName]
		and Encyclopedia.TrackerTables.characters_bNames[pType] then
			UpdateCompletionMarks(Encyclopedia.CompletionNotes_b[pName].Unlocks, name)
		end
	end
end

local UnlockFunctions = {
	[LevelStage.STAGE4_2] = function(room, stageType, difficulty, desc) -- Heart / Mother
		if room:IsClear() then
			local Name
			if stageType >= 4 and desc.SafeGridIndex == -10 then -- 4 is STAGETYPE_REPENTANCE, but using the enum breaks in ab+.
				Name = "Mother"
			elseif stageType <= StageType.STAGETYPE_AFTERBIRTH then
				Name = "MomsHeart"
			end
		
			if Name then
				CompletionFunc(Name)
			end
		end
	end,
	[LevelStage.STAGE4_3] = function(room, stageType, difficulty, desc) -- Hush
		if room:IsClear() then
			local Name = "Hush"
		
			CompletionFunc(Name)
		end
	end,
	[LevelStage.STAGE5] = function(room, stageType, difficulty, desc) -- Satan / Isaac
		if room:IsClear() then
			local Name = "Satan"
			if stageType == StageType.STAGETYPE_WOTL then
				Name = "Isaac"
			end
		
			CompletionFunc(Name)
		end
	end,
	[LevelStage.STAGE6] = function(room, stageType, difficulty, desc) -- Mega Satan / Lamb / Blue Baby
		if desc.SafeGridIndex == -7 then
			local MegaSatan
			for _, satan in ipairs(Isaac.FindByType(EntityType.ENTITY_MEGA_SATAN_2, 0)) do
				MegaSatan = satan
				break
			end
		
			if not MegaSatan then return end
			
			local sprite = MegaSatan:GetSprite()
			
			if sprite:IsPlaying("Death") and sprite:GetFrame() == 110 then
				local Name = "MegaSatan"
			
				CompletionFunc(Name)
			end
		else
			if room:IsClear() then
				local Name = "Lamb"
				if stageType == StageType.STAGETYPE_WOTL then
					Name = "BlueBaby"
				end
			
				CompletionFunc(Name)
			end
		end
	end,
	[LevelStage.STAGE7] = function(room, stageType, difficulty, desc) -- Delirium
		if desc.Data.Subtype == 70 and room:IsClear() then
			local Name = "Delirium"
		
			CompletionFunc(Name)
		end
	end,
	
	BossRush = function(room, stageType, difficulty, desc) -- Boss Rush
		if room:IsAmbushDone() then
			local Name = "BossRush"
		
			CompletionFunc(Name)
		end
	end,
	Beast = function(room, stageType, difficulty, desc) -- Beast
		local Beast
		for _, beast in ipairs(Isaac.FindByType(EntityType.ENTITY_BEAST, 0)) do
			Beast = beast
			break
		end
	
		if not Beast then return end
		
		local sprite = Beast:GetSprite()
		
		if sprite:IsPlaying("Death") and sprite:GetFrame() == 30 then
			local Name = "Beast"
		
			CompletionFunc(Name)
		end
	end,
	Greed = function(room, stageType, difficulty, desc) -- Greed
		if room:IsClear() then
			local Name = "GreedMode"
			
			CompletionFunc(Name)
		end
	end,
}

Encyclopedia:AddCallback(ModCallbacks.MC_POST_UPDATE, function()
	local level = game:GetLevel()
	local room = game:GetRoom()
	local desc = level:GetCurrentRoomDesc()
	local levelStage = level:GetStage()
	local roomType = room:GetType()
	local difficulty = game.Difficulty
	
	if difficulty <= Difficulty.DIFFICULTY_HARD then
		local stageType = level:GetStageType()
	
		if roomType == RoomType.ROOM_BOSS and UnlockFunctions[levelStage] then
			UnlockFunctions[levelStage](room, stageType, difficulty, desc)
		elseif roomType == RoomType.ROOM_BOSSRUSH then
			UnlockFunctions.BossRush(room, stageType, difficulty, desc)
		elseif levelStage == 13 and roomType == RoomType.ROOM_DUNGEON then -- 13 is STAGE8 but using the enum breaks AB+
			UnlockFunctions.Beast(room, stageType, difficulty, desc)
		end
	else
		if levelStage == LevelStage.STAGE7_GREED
		and roomType == RoomType.ROOM_BOSS
		and desc.SafeGridIndex == 45
		then
			UnlockFunctions.Greed(room, nil, difficulty, desc)
		end
	end
end)

function Encyclopedia.CreateUnlocksTemplate(name, Type, pID)
	if Encyclopedia["CompletionNotes_" .. Type][name] then return end
	
	local UnlockTab = {}
	
	for i, v in pairs(Unlocks) do
		UnlockTab[i] = Unlocks[i]
	end
	
	Encyclopedia["CompletionNotes_" .. Type][name] = {
		--Player = pID,
		Unlocks = UnlockTab,
	}
	
	Isaac.DebugString("[Encyclopedia] autogenerated (" .. Type .. "): " .. pID .. "." .. name)
end
