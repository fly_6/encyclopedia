Encyclopedia is now open source!

Now there are just a couple guidelines:
- DO NOT REUPLOAD THE MOD! I'm serious, while it's open source I'd still like it to only be a single Encyclopedia version on the workshop for simplicity's sake, we don't want to have the same issues as Mod Config Menu. If I ever stop support 100%, I'll do a public service announcement and update the workshop page.
- While the mod is open source, I will still be supervising all issues and merge requests. Even though I won't "support" the mod myself, I will however manually verify that all the changes you made work properly, otherwise the merge request would be rejected.
- If you are going to change the mod drastically (i.e. adding entire new submenus or reworking a whole system), I'd really appreciate it if you hit me up on discord (AgentCucco#6086) first to discuss what you wanna do. There are some systems that I'd like the mod to have, but I'd rather wait until they can be implemented properly instead of by using super hacky workarounds. So basically it's just a discussion to see what you wanna do and how, so I can give you the greenlight, as opposed to wasting your time if you submit it and I just reject it for the prior reasons.
- Submit a changelog with all the changes you made in your update! Since I will stil be in charge of manually updating the mod to the workshop, I'm gonna need those patchnotes.

I think that's all, aside from that, just have common sense (aka don't just delete the mod and try to merge request, lol), and any positive changes are welcome, no matter how small they are, even if it's just a single bug fix!
